#!/bin/sh

set -e

#
# Set default values for parameters
#
INIT_FILE="/var/www/dynamic_cfg/initialised"
INIT=0
SLEEP=5

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --init)
        INIT=1
        ;;
    *)
        ;;
esac
shift # past argument or value
done

#
# Run script
#

#Wait for certificate generation
#while  [ ! -f  '/var/www/TLS_key_store/initialised' ]; do echo 'waiting for certificate generation'; sleep 5; done

#wait for the database to start
while ! echo exit | nc ${MARIADB_HOST} 3306 </dev/null; do echo 'Waiting for database to become available'; sleep "${SLEEP}"; done

if [ ${INIT} -eq 1 ]; then
    if [ ! -f  "${INIT_FILE}" ]; then
        echo "Initialization started"

        sh -x /usr/local/bin/alpine-httpd__init_entrypoint.sh

        python3 -m 'template_substitute' _ADDITIONAL_SERVERNAME \
            --input '/etc/apache2/conf.d/additional_vhosts.conf' \
            --output '/var/www/dynamic_cfg/additional_vhosts.conf'

        ## Check whether the required data is mounted via host volumes.
        if [ -f '/srv/clarin_eric_website/database.sql' ] && \
           [ -f '/srv/clarin_eric_website/htdocs.tar.gz' ]; then

            python3 -m 'alpine_httpd_drupal_restore' --restore

            #Append to the end of the settings file
            #See: http://stackoverflow.com/a/13864829
            if [ ! -z ${PROXY_URL+x} ]; then
                #TODO: prevent repeatedly adding this section to the config
                echo "Configuring proxy setup"
                {
                    echo "\$base_url = '""${PROXY_URL}""';" ;
                    # See: https://www.karelbemelmans.com/2015/04/reverse-proxy-configuration-for-drupal-7-sites/
                    echo "\$conf['reverse_proxy'] = TRUE;" ;
                    echo "\$conf['reverse_proxy_header'] = 'HTTP_X_FORWARDED_FOR';" ;
                    # See: https://groups.drupal.org/node/283213
                    echo "\$conf['reverse_proxy_addresses'] = array(\$_SERVER['REMOTE_ADDR']);" ;
                } >> /var/www/localhost/htdocs/sites/default/settings.php
            fi

            httpd -e DEBUG -t
        else
            printf '%s\n' "WARNING: Initializing container from scratch, without data
            hasn't been implemented yet. Assuming no initalization is needed. "
            # printf '%s\n' \
            #   'WARNING: Restoring Drupal table failed, created empty table. '
            # TODO: drush.phar drush sql-create
        fi

        #Write initialization state to disk
        touch "${INIT_FILE}"
    else
        echo "${INIT_FILE} exists, skipping initialisation"
    fi
else
     while  [ ! -f  "${INIT_FILE}" ]; do echo "Waiting for preparer to finish"; sleep "${SLEEP}"; done

    #Start apache webserver
    dumb-init --single-child httpd -D FOREGROUND
fi
