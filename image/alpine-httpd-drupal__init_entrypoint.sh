#!/bin/sh
set -e
python3 -m 'template_substitute' _ADDITIONAL_SERVERNAME --input \
    '/etc/apache2/conf.d/additional_vhosts.conf' \
    --output '/var/www/dynamic_cfg/additional_vhosts.conf'
## Check whether the required data is mounted via host volumes.
## Because of Docker Engine's odd behavior to create an empty directory for
## nonexistent host volume paths, use an elaborate check whether any files \
## exist under '/srv/clarin_eric_website/www-clarin-eu_src/'.
if [ -f '/srv/clarin_eric_website/database.sql' ] && \
	[ -f '/srv/clarin_eric_website/htdocs.tar.gz' ]; then
	python3 -m 'alpine_httpd_drupal_restore' --restore

	#Append to the end of the settings file
	#See: http://stackoverflow.com/a/13864829
    if [ ! -z ${PROXY_URL+x} ]; then
        #TODO: prevent repeatedly adding this section to the config
        echo "Configuring proxy setup"
        {
            echo "\$base_url = '""${PROXY_URL}""';" ;
            # See: https://www.karelbemelmans.com/2015/04/reverse-proxy-configuration-for-drupal-7-sites/
            echo "\$conf['reverse_proxy'] = TRUE;" ;
            echo "\$conf['reverse_proxy_header'] = 'HTTP_X_FORWARDED_FOR';" ;
            # See: https://groups.drupal.org/node/283213
            echo "\$conf['reverse_proxy_addresses'] = array(\$_SERVER['REMOTE_ADDR']);" ;
        } >> /var/www/localhost/htdocs/sites/default/settings.php
    fi
    httpd -e DEBUG -t
else
    printf '%s\n' "WARNING: Initializing container from scratch, without data
    hasn't been implemented yet. Assuming no initalization is needed. "
    # printf '%s\n' \
    #   'WARNING: Restoring Drupal table failed, created empty table. '
    # TODO: drush.phar drush sql-create
fi
