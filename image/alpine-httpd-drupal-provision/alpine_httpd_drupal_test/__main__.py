#!/usr/bin/python3
from contextlib import closing
from http import HTTPStatus
from http.client import HTTPConnection
from http.client import HTTPResponse
from http.client import HTTPSConnection
from os import environ
from pathlib import Path
from shlex import quote
from ssl import _create_unverified_context
from ssl import create_default_context
from ssl import SSLContext
from subprocess import run
from unittest import main
from unittest import TestCase
from urllib.parse import urlparse

# TODO: Do not use globals.
VERBOSITY = 0
HTTP_PORT = 8080
HTTPS_PORT = 4430
DO_VERIFY_TLS = False


def _getresponse(conn: HTTPConnection,
                 host_header_field_value: str,
                 path: str='/') -> HTTPResponse:
    conn.set_debuglevel(VERBOSITY)
    conn.putrequest('GET', path, skip_host=True)
    conn.putheader('Host', host_header_field_value)
    conn.endheaders()
    return conn.getresponse()


class TestDrupal(TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.servername = environ['_SERVERNAME'].strip()
        self.htdocs_dir_path = Path('/var/www/localhost/htdocs/')

    def _test_redirect(self,
                       response: HTTPResponse,
                       fqdn: str=None,
                       context: SSLContext=None) -> None:
        fqdn = self.servername if not fqdn else fqdn
        self.assertEqual(response.status, HTTPStatus.MOVED_PERMANENTLY)
        url = urlparse(response.getheader('Location'))
        self.assertEqual(url.scheme, 'https')
        try:
            self.assertEqual(url.netloc, fqdn)
        except AssertionError:
            self.assertEqual(url.netloc, fqdn + ':' + str(HTTPS_PORT))

        # Continue if redirect is to self.servername, otherwise stop test.
        if url.netloc == self.servername:
            with closing(
                    HTTPSConnection(
                        url.netloc, HTTPS_PORT, context=context)) as conn:
                response = _getresponse(
                    conn,
                    fqdn,
                    path=url.path + url.params + url.query + url.fragment)
            self.assertEqual(response.status, HTTPStatus.OK)

    def test_01_drush(self) -> None:
        """Test whether Drush works.
        """

        run('drush.phar --root=' + quote(str(self.htdocs_dir_path)) +
            ' core-status',
            shell=True,
            check=True)

    def test_02_http_to_https(self) -> None:
        """http://_SERVERNAME/ -> https://_SERVERNAME/ ✓.
        """

        with closing(HTTPConnection(self.servername, HTTP_PORT)) as conn:
            response = _getresponse(conn, self.servername)
        self._test_redirect(response)

    def test_03_correct(self) -> None:
        """https://_SERVERNAME/ ✓.
        """
        context = create_default_context() if DO_VERIFY_TLS else \
            _create_unverified_context()
        with closing(
                HTTPSConnection(
                    self.servername, HTTPS_PORT, context=context)) as conn:
            response = _getresponse(conn, self.servername)
        self.assertEqual(response.status, HTTPStatus.OK)

    def test_04_http_incorrect(self) -> None:
        """http://INCORRECT/ ✗.
        """

        with closing(HTTPConnection(self.servername, HTTP_PORT)) as conn:
            response = _getresponse(conn, 'invalid.invalid')
        self.assertEqual(response.status, HTTPStatus.BAD_REQUEST)

    def test_05_https_incorrect(self) -> None:
        """https://INCORRECT/ ✗.
        """
        context = create_default_context() if DO_VERIFY_TLS else \
            _create_unverified_context()
        with closing(
                HTTPSConnection(
                    self.servername, HTTPS_PORT, context=context)) as conn:
            response = _getresponse(conn, 'invalid.invalid')
        self.assertEqual(response.status, HTTPStatus.BAD_REQUEST)

    def test_06_http_legacy(self) -> None:
        """http://clarin.eu/ -> https://_SERVERNAME/ ✓.
        """

        with closing(HTTPConnection(self.servername, HTTP_PORT)) as conn:
            response = _getresponse(conn, 'clarin.eu')
        self._test_redirect(response)

    def test_07_https_legacy(self) -> None:
        """https://clarin.eu/ -> https://_SERVERNAME/ ✓.
        """
        context = create_default_context() if DO_VERIFY_TLS else \
            _create_unverified_context()
        with closing(
                HTTPSConnection(
                    self.servername, HTTPS_PORT, context=context)) as conn:
            response = _getresponse(conn, 'clarin.eu')
        self._test_redirect(response)

    def test_08_aai_infra(self) -> None:
        """https:///_SERVERNAME/aai -> https://infra.clarin.eu/aai ✓.
        """
        context = create_default_context() if DO_VERIFY_TLS else \
            _create_unverified_context()
        with closing(
                HTTPSConnection(
                    self.servername, HTTPS_PORT, context=context)) as conn:
            response = _getresponse(conn, self.servername, '/aai')
        self._test_redirect(response, 'infra.clarin.eu')

    def test_09_cmd_infra(self) -> None:
        """https://_SERVERNAME/cmd -> https://infra.clarin.eu/cmd ✓.
        """
        context = create_default_context() if DO_VERIFY_TLS else \
            _create_unverified_context()
        with closing(
                HTTPSConnection(
                    self.servername, HTTPS_PORT, context=context)) as conn:
            response = _getresponse(conn, self.servername, '/cmd')
        self._test_redirect(response, 'infra.clarin.eu')

    def test_10_aai_infra_deeper(self) -> None:
        """https://_SERVERNAME/aai/prod_md_about_clarin_erics_idp.xml ->
        https://infra.clarin.eu/aai/prod_md_about_clarin_erics_idp.xml ✓.
        """
        context = create_default_context() if DO_VERIFY_TLS else \
            _create_unverified_context()
        with closing(
                HTTPSConnection(
                    self.servername, HTTPS_PORT, context=context)) as conn:
            response = _getresponse(conn, self.servername,
                                    '/aai/prod_md_about_clarin_erics_idp.xml')
        self._test_redirect(response, 'infra.clarin.eu')

    def test_11_cmd_infra_deeper(self) -> None:
        """https://_SERVERNAME/cmd/general-component-schema.xsd ->
        https://_SERVERNAME/cmd/general-component-schema.xsd ✓.
        """
        context = create_default_context() if DO_VERIFY_TLS else \
            _create_unverified_context()
        with closing(
                HTTPSConnection(
                    self.servername, HTTPS_PORT, context=context)) as conn:
            response = _getresponse(conn, self.servername,
                                    '/cmd/general-component-schema.xsd')
        self._test_redirect(response, 'infra.clarin.eu')


if __name__ == '__main__':
    main()
