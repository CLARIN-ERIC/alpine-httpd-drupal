#!/usr/bin/python3
from os.path import isfile
from os.path import isdir
from os import environ
from pathlib import Path
from shlex import quote
from string import Template
from logging import debug
import argparse

from provisioning import provision
from provisioning import sh_cmd


def restore_htdocs(htdocs_dir_path: Path):
    """Restore Drupal content in htdocs/.
    """

    htdocs_archive_file_path = Path('/srv/clarin_eric_website/htdocs.tar.gz')
    sh_cmd('tar --auto-compress --directory={htdocs_dir_path:s} --extract '
           "--file={htdocs_archive_file_path:s} --format='posix' --overwrite "
           '--same-permissions'.format(
               htdocs_dir_path=quote(str(htdocs_dir_path)),
               htdocs_archive_file_path=quote(str(htdocs_archive_file_path))))


def restore_htdocs_perms(htdocs_dir_path: Path) -> None:
    """Restore htdocs/ ownership and permissions filesystem metadata.
    """

    htdocs_dir_path_str = quote(str(htdocs_dir_path))
    # An iterable of all files and directories under sites/*/files/.
    files_paths_glob_str = ' '.join(
        quote(str(path)) for path in htdocs_dir_path.glob('sites/*/files*/**'))
    sh_cmd('chown -R 0:www-data -- ' + htdocs_dir_path_str)
    sh_cmd('find ' + htdocs_dir_path_str +
           " -type d -exec chmod u=rwx,g=rsx,o= -- '{}' +")
    sh_cmd('find ' + htdocs_dir_path_str +
           " -type f -exec chmod u=rw,g=r,o= -- '{}' +")
    sh_cmd('chmod ug+w -- {:s}'.format(files_paths_glob_str))


def customize_drupal(cfg_archive_file_path: Path, custom_src_root_dir_path:
                     Path, htdocs_dir_path: Path, mariadb_host: str,
                     mysql_database: str, mysql_password: str, mysql_user: str,
                     theme_src_dir_path: Path) -> None:
    """Customize a Drupal 7 installation's modules, settings and themes.
    """
    modules_path_str = 'sites/all/modules/custom/'
    themes_dir_path = htdocs_dir_path.joinpath('sites/all/themes/')

    file_paths_dict = {
        'cfg_archive_file_path': cfg_archive_file_path,
        'cfg_file_path':
        htdocs_dir_path.joinpath('sites/default/settings.php'),
        'htdocs_dir_path': htdocs_dir_path,
        'modules_src_dir_path':
        custom_src_root_dir_path.joinpath(modules_path_str),
        'modules_target_dir_path': htdocs_dir_path.joinpath(modules_path_str),
        'theme_src_dir_path': theme_src_dir_path,
        'theme_target_dir_path':
        themes_dir_path.joinpath(theme_src_dir_path.stem),
        'themes_dir_path': themes_dir_path,
    }

    file_path_strs_dict = {name: quote(str(path))
                           for name, path in file_paths_dict.items()}

    sh_cmd('tar --auto-compress --directory={htdocs_dir_path:s} --extract '
           '--file={cfg_archive_file_path:s} --overwrite --same-permissions '
           "--strip-components='1'".format(**file_path_strs_dict))

    with file_paths_dict['cfg_file_path'].open(
            mode='rt') as drupal_settings_file:
        settings_template_str = drupal_settings_file.read()
    settings_template = Template(settings_template_str)
    settings_concrete_str = settings_template.safe_substitute(
        MARIADB_HOST=mariadb_host,
        MYSQL_DATABASE=mysql_database,
        MYSQL_PASSWORD=mysql_password,
        MYSQL_USER=mysql_user)
    with file_paths_dict['cfg_file_path'].open(
            mode='wt') as drupal_settings_file:
        drupal_settings_file.write(settings_concrete_str)

    # Remove customizations at targets.
    sh_cmd('rm -r -- {modules_target_dir_path:s} '
           '{themes_dir_path:s}'.format(**file_path_strs_dict))
    # Add customizations at targets based on sources.
    sh_cmd('cp -r -- {modules_src_dir_path:s} '
           '{modules_target_dir_path:s}'.format(**file_path_strs_dict))
    sh_cmd('mkdir -- {themes_dir_path:s}'.format(**file_path_strs_dict))
    sh_cmd('cp -r -- {theme_src_dir_path:s} {theme_target_dir_path:s}'
           .format(**file_path_strs_dict))
    # At this point the theme needs to be enabled still.
    # See: enable_drupal_theme().


def restore_drupal_database(mariadb_host: str, mysql_database: str,
                            mysql_password: str, mysql_user: str) -> None:
    """Restore Drupal 7 database from MariaDB dump file, if any.
    """

    database_dump_file_path = Path('/srv/clarin_eric_website/database.sql')

    if database_dump_file_path.absolute():
        sh_cmd(
            "mysql --user='{mysql_user:s}' --database='{mysql_database:s}' "
            "--host='{mariadb_host:s}' --port='3306'".format(
                mariadb_host=mariadb_host,
                mysql_database=mysql_database,
                mysql_user=mysql_user),
            env={'MYSQL_PWD': mysql_password},
            stdin=database_dump_file_path.open(mode='rb'))
    else:
        raise RuntimeError('Nothing found at {:s}. ',
                           quote(str(database_dump_file_path)))


def enable_drupal_theme(theme_name: str, htdocs_dir_path_str: str) -> None:
    sh_cmd(
        "drush.phar -y en bootstrap '{theme_name:s}'".format(
            theme_name=theme_name),
        cwd=htdocs_dir_path_str)
    sh_cmd(
        "drush.phar vset admin_theme '{theme_name:s}'".format(
            theme_name=theme_name),
        cwd=htdocs_dir_path_str)
    sh_cmd(
        "drush.phar vset default_theme '{theme_name:s}'".format(
            theme_name=theme_name),
        cwd=htdocs_dir_path_str)
    sh_cmd('drush.phar cache-clear all', cwd=htdocs_dir_path_str)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--restore", help="Restore backup from filesystem", action="store_true")
    parser.add_argument("-c", "--customize", help="Apply specific customizations from filesystem", action="store_true")
    args = parser.parse_args()

    restore = args.restore
    customize = args.customize

    debug('Options:')
    debug('\tRestore: '+str(restore))
    debug('\tCustomize: '+str(customize))

    with provision():
        WEB_ROOT_DIR_PATH = Path('/var/www/localhost/')
        HTDOCS_DIR_PATH = WEB_ROOT_DIR_PATH.joinpath('htdocs/')

        MARIADB_HOST = environ['MARIADB_HOST'].strip()
        assert MARIADB_HOST

        MYSQL_DATABASE = environ['MYSQL_DATABASE'].strip()
        assert MYSQL_DATABASE

        MYSQL_PASSWORD = environ['MYSQL_PASSWORD'].strip()
        assert MYSQL_PASSWORD
        del environ['MYSQL_PASSWORD']

        MYSQL_USER = environ['MYSQL_USER'].strip()
        assert MYSQL_USER

        READ_ONLY = 0
        if 'READ_ONLY' in environ:
            READ_ONLY = int(environ['READ_ONLY'].strip())

        if not restore and not customize:
            raise RuntimeError('At least one (or more) of --restore or --customize must be supplied.')

        htdocsTarball = '/srv/clarin_eric_website/htdocs.tar.gz'
        dbDumpFile = '/srv/clarin_eric_website/database.sql'
        configTarball = '/srv/clarin_eric_website/www-clarin-eu_cfg.tar.gz'
        sourceDir = '/srv/clarin_eric_website/www-clarin-eu_src/'

        htdocsTarballIsValid = isfile(htdocsTarball)
        dbDumpFileIsValid = isfile(dbDumpFile)
        configTarballIsValid = isfile(configTarball)
        sourceDirIsValid = isdir(sourceDir)

        debug('Options:')
        debug('\tRestore: '+str(restore))
        debug('\tCustomize: '+str(customize))
        debug('Input files:')
        if restore:
            if not htdocsTarballIsValid:
                debug('\t'+htdocsTarball+': not found.')
            else:
                debug('\t'+htdocsTarball+': ok.')

            if not dbDumpFileIsValid:
                debug('\t'+dbDumpFile+': not found.')
            else:
                debug('\t'+dbDumpFile+': ok.')

            if not htdocsTarballIsValid and not dbDumpFileIsValid:
                raise RuntimeError('Restore cannot continue. Aborting.')

        if customize:
            if not configTarballIsValid:
                debug('\t'+configTarball+': not found.')
            else:
                debug('\t'+configTarball+': ok.')

            if not sourceDirIsValid:
                debug('\t'+sourceDir+': not found or not a directory.')
            else:
                debug('\t'+sourceDir+': ok.')

            if not configTarballIsValid or not sourceDirIsValid:
                debug('Skipping application of customizations (cfg + src)')

        if restore:
            debug('Restoring htdocs')
            restore_htdocs(htdocs_dir_path=HTDOCS_DIR_PATH)

        if customize and configTarballIsValid and sourceDirIsValid:
            debug('Config and source found. Applying customizations')
            customize_drupal(
                cfg_archive_file_path=Path(configTarball),
                custom_src_root_dir_path=Path(sourceDir),
                htdocs_dir_path=HTDOCS_DIR_PATH,
                mariadb_host=MARIADB_HOST,
                mysql_database=MYSQL_DATABASE,
                mysql_password=MYSQL_PASSWORD,
                mysql_user=MYSQL_USER,
                theme_src_dir_path=Path('/srv/clarin_eric_website/CLARIN_Horizon/'))
        else:
            #Override database connection settings when importing backup only
            #TODO: check if a python based approach works better
            file="/var/www/localhost/htdocs/sites/default/settings.php"
            sh_cmd("sed -i -E \"s/(.*)'database' => '(.*)',/'database' => '"+MYSQL_DATABASE+"',/g\" "+file)
            sh_cmd("sed -i -E \"s/(.*)'host' => '(.*)',/'host' => '"+MARIADB_HOST+"',/g\" "+file)
            sh_cmd("sed -i -E \"s/(.*)'password' => '(.*)',/'password' => '"+MYSQL_PASSWORD+"',/g\" "+file)
            sh_cmd("sed -i -E \"s/(.*)'username' => '(.*)',/'username' => '"+MYSQL_USER+"',/g\" "+file)

        if restore:
            debug('Restoring htdocs permissions')
            restore_htdocs_perms(htdocs_dir_path=HTDOCS_DIR_PATH)

        if restore:
            debug('Restoring database')
            restore_drupal_database(
                mariadb_host=MARIADB_HOST,
                mysql_database=MYSQL_DATABASE,
                mysql_password=MYSQL_PASSWORD,
                mysql_user=MYSQL_USER)

        if customize:
            enable_drupal_theme(
                theme_name='CLARIN_Horizon',
                htdocs_dir_path_str=str(HTDOCS_DIR_PATH))

        if restore:
            if READ_ONLY:
                debug('Setting site to read-only')
                sh_cmd('drush.phar vset site_readonly 1')
