#!/usr/bin/python3
from locale import getpreferredencoding
from logging import debug
from pathlib import Path
from re import fullmatch
from shlex import quote
from subprocess import PIPE

from provisioning import provision
from provisioning import sh_cmd


def configure_apache_httpd() -> None:
    httpd_cfg_file_path = Path('/etc/apache2/httpd.conf')
    with httpd_cfg_file_path.open(mode='rt+') as httpd_config_file:
        httpd_config_lines = httpd_config_file.readlines()
        httpd_config_file.seek(0)
        l_act = r'\s*'
        l_any = r'[\s#]*'
        r_val = r'\s+[^\s#]+\n'
        for line in httpd_config_lines:
            if fullmatch(l_act + r'(?:LoadModule\s+mpm_event_module|'
                         r'LoadModule\s+mpm_worker_module)' + r_val, line):
                httpd_config_file.write('#' + line)
            elif fullmatch(l_any + r'LoadModule\s+mpm_prefork_module' + r_val,
                           line):
                httpd_config_file.write(line[1:])
            else:
                httpd_config_file.write(line)
        httpd_config_file.truncate()
    sh_cmd("chmod -R 'ug=rX,u+w' -- '/etc/apache2/conf.d/'")


def install_drush_directly(executable_url: str,
                           sha256_val: str,
                           drush_dir_path:
                           Path=Path('/usr/local/bin/')) -> None:
    sh_cmd(
        "curl --fail --location --proto '=https' --output 'drush.phar' "
        '--show-error --silent --tlsv1.2 ' + quote(executable_url),
        cwd=str(drush_dir_path))
    sh_cmd(
        "printf '{sha256_val:s} *drush.phar\n' | sha256sum -c -s -"
        .format(sha256_val=sha256_val),
        cwd='/usr/local/bin/')
    sh_cmd("chmod 'a+x' -- 'drush.phar'", cwd=str(drush_dir_path))


def install_packages() -> None:
    sh_cmd('apk --quiet update --update-cache 1>&2', stdout=PIPE)
    completed_process = sh_cmd(
        'apk add --upgrade '
        'mariadb-client==10.1.22-r0 '
        'php5==5.6.31-r0 '
        'php5-apache2==5.6.31-r0 '
        'php5-cli==5.6.31-r0 '
        'php5-ctype==5.6.31-r0 '
        'php5-curl==5.6.31-r0 '
        'php5-dom==5.6.31-r0 '
        'php5-gd==5.6.31-r0 '
        'php5-json==5.6.31-r0 '
        'php5-ldap==5.6.31-r0 '
        'php5-mysqli==5.6.31-r0 '
        'php5-openssl==5.6.31-r0 '
        'php5-pdo_mysql==5.6.31-r0 '
        'php5-phar==5.6.31-r0 '
        'php5-xml==5.6.31-r0 '
        'sudo==1.8.16-r0 '
        'tar==1.29-r1 1>&2',
        stdout=PIPE)
    debug(
        completed_process.stderr.decode(encoding=getpreferredencoding(False)))


if __name__ == '__main__':
    with provision():
        configure_apache_httpd()

        install_packages()

        install_drush_directly(
            executable_url='https://github.com/drush-ops/drush/releases/'
            'download/8.1.6/drush.phar',
            sha256_val='a1fe00aadb917a6ba83a3daf95a5e1044ee0575d9bc88d73dc2e92'
            '12a1ed3a0d')

        sh_cmd("cp -- '/tmp/ldap.conf' '/etc/openldap/ldap.conf'")
        sh_cmd("chmod -R 'a=r,u+w' -- '/etc/openldap/ldap.conf'")
