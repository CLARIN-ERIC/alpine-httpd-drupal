#!/bin/sh

set -e

## `website_extra.conf.template` does not really require templating in this
## image. This may be different in dependent images.
cp -- '/etc/apache2/conf.d/website_extra.conf.template'  '/var/www/dynamic_cfg/website_extra.conf'
mv '/etc/apache2/conf.d/auth.conf' '/var/www/dynamic_cfg/'

python3 -m 'template_substitute' _SERVERNAME --input \
    '/etc/apache2/conf.d/website.conf.template' \
    --output '/var/www/dynamic_cfg/website.conf'
python3 -m 'template_substitute' _ADDITIONAL_SERVERNAME --input \
    '/etc/apache2/conf.d/additional_vhosts.conf' \
    --output '/var/www/dynamic_cfg/additional_vhosts.conf'


cd -- '/var/www/TLS_key_store/'

## Generate Diffie Helman parameters if the file doesn't exist.
## -dsaparam: http://blog.intothesymmetry.com/2016/01/openssl-key-recovery-attack-on-dh-small.html
if [ ! -f '/var/www/TLS_key_store/dhparam.pem' ]; then
    openssl dhparam -dsaparam -out 'dhparam.pem' 2048 2> /dev/null
    chown apache:apache -c -- 'dhparam.pem'
fi

## Link certificate if no private key file is available.
if [ ! -f '/var/www/TLS_key_store/private_nopass.key' ]; then
    ln -fs -- '/etc/ssl/apache2/server.pem' 'bundle.pem'
    ln -fs -- '/etc/ssl/apache2/server.key' 'private_nopass.key'
fi
